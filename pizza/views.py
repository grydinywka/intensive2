from django.shortcuts import render, render_to_response
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext

from .models import Customer

@staff_member_required
def my_admin_view_for_customer(request):
    c_r = Customer.objects.get(name='San Sanick')
    val_orders = c_r.get_value_orders()
    return render_to_response('my_template.html', context_instance=RequestContext(request), dictionary={
        "val_orders": val_orders,
        "val_orders2": c_r})