from django.contrib import admin

from .models import Product, Customer, Courier, StatusOrder, Order, QuantityProduct

admin.site.register(Product)
admin.site.register(Customer)
admin.site.register(Courier)
admin.site.register(StatusOrder)
admin.site.register(QuantityProduct)
admin.site.register(Order)

