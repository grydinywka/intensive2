from __future__ import unicode_literals

from django.db import models

import decimal

def most_pop_pizza(queryset):
    pizzas = {}
    for pizza in Product.objects.filter(type='pizza'):
        pizzas[pizza.name] = 0
    for orders in queryset:
        for qp in orders.quantity_product.all():
            if qp.product.type == 'pizza':
                pizzas[qp.product.name] += qp.quantity

    max_order_by_pizza = 0
    if len(pizzas):
        pop_pizza = pizzas.keys()[0]
        for key in pizzas.keys():
            if max_order_by_pizza < pizzas[key]:
                max_order_by_pizza = pizzas[key]
                pop_pizza = key
    else:
        pop_pizza = ''


    return pop_pizza

class Product(models.Model):
    type = models.CharField(
        max_length=256,
        blank=False,
        choices=(
            ('pizza', 'pizza'),
            ('beverage', 'beverage'),
            ('sweets', 'sweets'),
        )
    )
    name = models.CharField(max_length=254, blank=False, null=True, default=None)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    @classmethod
    def most_popular_pizza(cls_obj):
        qs = Order.objects.all()
        return most_pop_pizza(qs)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"

class Customer(models.Model):
    name = models.CharField(max_length=254, blank=False, null=True, default=None)
    address = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_value_orders(self):
        orders = Order.objects.filter(customer=self)
        return orders.count()

    def get_favorite_pizza(self):
        qs = self.order_set.all()
        return most_pop_pizza(qs)

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"

class Courier(models.Model):
    name = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = "Courier"
        verbose_name_plural = "Couriers"

class StatusOrder(models.Model):
    name = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = "StatusOrder"
        verbose_name_plural = "StatusOrders"

class QuantityProduct(models.Model):
    product = models.ForeignKey('Product', blank=False, null=True, default=None)
    quantity = models.IntegerField(blank=False, null=True, default=1)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"Product - %s, quantity: %s" % (self.product.name, self.quantity)

    class Meta:
        verbose_name = "QuantityProduct"
        verbose_name_plural = "QuantityProduct"

class Order(models.Model):
    status_order = models.ForeignKey('StatusOrder', blank=False, null=True, default=None)
    customer = models.ForeignKey('Customer', blank=False, null=True, default=None)
    courier = models.ForeignKey('Courier', blank=False, null=True, default=None)
    # product = models.ManyToManyField('Product', blank=False, default=None)
    quantity_product = models.ManyToManyField('QuantityProduct', blank=False, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"Order #%s from Customer %s" % (self.pk, self.customer)

    def price(self):
        whole_price = 0
        for item in self.quantity_product.all():
            whole_price += (item.product.price * item.quantity)
        return whole_price

    def price_special_offer(self):
        val_order = self.customer.get_value_orders() + 1
        whole_price = self.price()

        if val_order <= 20:
            return whole_price * decimal.Decimal(0.97)
        if val_order <= 100:
            return whole_price * decimal.Decimal(0.96)
        return whole_price * decimal.Decimal(0.95)

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"