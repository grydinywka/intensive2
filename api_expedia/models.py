from __future__ import unicode_literals

from django.db import models

class ThingToDo(models.Model):
    city=models.CharField(max_length=254, blank=False, null=True, default=None)
    startDate=models.DateField(blank=False, null=True, default=None)
    endDate=models.DateField(blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.city

    class Meta:
        verbose_name = "ThingToDo"
        verbose_name_plural = "ThingsToDo"
