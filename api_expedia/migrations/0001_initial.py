# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-07 10:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ThingToDo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.CharField(default=None, max_length=254, null=True)),
                ('startDate', models.DateField(blank=True, default=None, null=True)),
                ('endDate', models.DateField(blank=True, default=None, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'ThingToDo',
                'verbose_name_plural': 'ThingsToDo',
            },
        ),
    ]
