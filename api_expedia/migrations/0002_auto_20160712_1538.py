# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-12 15:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_expedia', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thingtodo',
            name='endDate',
            field=models.DateField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='thingtodo',
            name='startDate',
            field=models.DateField(default=None, null=True),
        ),
    ]
