from __future__ import unicode_literals

from django.apps import AppConfig


class ApiExpediaConfig(AppConfig):
    name = 'api_expedia'
