# coding=utf-8
from django.shortcuts import render
from django import forms
from django.core.urlresolvers import reverse

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field
from crispy_forms.bootstrap import FormActions

from django.views.generic import UpdateView, ListView, CreateView
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from django.http import JsonResponse

from .models import ThingToDo

import datetime
import time
import requests

from . import tasks

def delay(request):
    tasks.warsaw.delay()
    return HttpResponse('<p>Delay celery</p>')

class GetThingToDoForm(forms.ModelForm):
    class Meta:
        model = ThingToDo
        fields = '__all__'
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(GetThingToDoForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)

        # set form tag attributes
        self.helper.form_action = reverse('thtodo')
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-horizontal'

        # set form field properties
        self.helper.help_text_inline = True
        self.helper.html5_required = False
        self.helper.label_class = 'col-sm-2 control-label'
        self.helper.field_class = 'col-sm-5'

        # add buttons

        self.helper.layout[-1] = FormActions(
            Submit('submit_button', u'Submit', css_class="btn btn-primary"),
            Submit('cancel_button', u'Reset', css_class="btn btn-link")
            )
    city = forms.CharField(
        label='City',
        initial='Warsaw',
        help_text='City for thing to do',

    )
    startDate=forms.DateField(
        label='Start date',
        initial=datetime.datetime.now().date().strftime("%Y-%m-%d"),
        help_text='Date of start things',
        # required=False,
        error_messages={'required': u"Field date of birth is mandatory",
                        'invalid': u"Input valid format of date"}

    )
    endDate=forms.DateField(
        label='End date',
        initial=(datetime.datetime.now().date() + datetime.timedelta(days=7)).strftime("%Y-%m-%d"),
        help_text='Date of end things',
        # required=False,
    )
    # endDate=models.DateField(blank=True, null=True, default=None)
    no_field = forms.CharField(required=False)


class GetThingToDoView(CreateView):
    """
    Expedia’s Activities API offers Search and Details functionality to explore “things to do”
    in a location of interest. Search API provides the list of activities with brief summary
    about each activity.
    To get more details for each activity, you can use the Details API.
    """
    model = ThingToDo
    template_name = 'thingtodo.html'
    form_class = GetThingToDoForm

    def get_success_url(self):
        return reverse('thtodo')

    def post(self, request, *args, **kwargs):
        # if request.POST.get('cancel_button'):
        #     return HttpResponseRedirect(reverse('index'))
        return super(GetThingToDoView, self).post(request, *args, **kwargs)

    def get_defauld_data(self):
        return {'cancel_button': True,
                'city': 'Warsaw',
                'start_date': datetime.datetime.now().date().strftime("%Y-%m-%d"),
                'end date': (datetime.datetime.now().date() + datetime.timedelta(days=7)).strftime("%Y-%m-%d")}

    def form_invalid(self, form):
        data = self.request.POST
        cancel = data.get('cancel_button', '')
        if cancel:
            return JsonResponse(self.get_defauld_data())
        # startDate = data.get('startDate')
        # endDate = data.get('endDate')
        # try:
        #     datetime.datetime.strptime(startDate, '%Y-%m-%d')
        #     datetime.datetime.strptime(endDate, '%Y-%m-%d')
        # except ValueError as e:
        #     return JsonResponse({'error': e})
        # print 'invalid method'
        if form.errors:
            return JsonResponse({'error': str(form.errors)})
        # return JsonResponse({'error': 'form.er'})
        return super(GetThingToDoView, self).form_invalid(form)

    def form_valid(self, form):
        data = self.request.POST

        if data.get('cancel_button'):
            return JsonResponse(self.get_defauld_data())

        city = data.get('city')
        startDate = data.get('startDate')
        endDate = data.get('endDate')
        try:
            datetime.datetime.strptime(startDate, '%Y-%m-%d')
            datetime.datetime.strptime(endDate, '%Y-%m-%d')
        except ValueError as e:
            return JsonResponse({'error': e})
        print 'valid method'
        INSERT_YOUR_API_KEY = 'WpsXCeFL9E2aEUGKiGAobsmTPlQL3Vcz'
        r = requests.get('http://terminal2.expedia.com/x/activities/search?location={'
                         '}&startDate={}&endDate={}&apikey={}'.format(city,
                                                                      startDate,
                                                                      endDate,
                                                                      INSERT_YOUR_API_KEY))
        if r.json().has_key("status"):
            return JsonResponse({'error': 'some error'})
        if time.strptime(endDate, '%Y-%m-%d').tm_year - time.strptime(startDate, '%Y-%m-%d').tm_year < 0:
            return JsonResponse({'error': 'year in endDate must be more or equal than in startDate'})
        if time.strptime(endDate, '%Y-%m-%d').tm_mon - time.strptime(startDate, '%Y-%m-%d').tm_mon < 0:
            return JsonResponse({'error': 'month in endDate must be more or equal than in startDate'})
        if time.strptime(endDate, '%Y-%m-%d').tm_mday - time.strptime(startDate, '%Y-%m-%d').tm_mday < 0:
            return JsonResponse({'error': 'day in endDate must be more or equal than in startDate'})

        return JsonResponse({'thing': r.json()['activities']})

    def get_context_data(self, **kwargs):
        context = super(GetThingToDoView, self).get_context_data(**kwargs)
        return context