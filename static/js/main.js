function getJsonOfThings() {
    var form = $('form');

    form.ajaxForm({
        dataType: "json",
        'beforeSend': function() {
            $('#spinner').css('display', 'block');
            $('#message').css('display', 'none').empty();
        },
        'error': function(){
            $('#spinner').css('display', 'none');
            alert('Error on server. Attempt later, please!1234');

            return false;
        },
        'success': function(data, status, xhr){
            var form = $(data).find('form');
            if ( data['error'] ) {
                $('#message').append(data['error']).css('display', 'block');
//                $('#message').append(data);
            } else if ( data['cancel_button'] ) {
                $('table').css('display', 'none');
//                $('input[name="city"]').css('display', 'none');
                $('input[name="city"]').val(data['city']);
                $('input[name="startDate"]').val(data['start_date']);
                $('input[name="endDate"]').val(data['end date']);
            } else {
                var len = data['thing'].length;

                $( "#found_thing" ).empty();

                for ( var i = 0; i < len; i++ ) {
                    var img_url = data['thing'][i]["imageUrl"];

                    img_url = img_url.replace(RegExp('^\/\/','g'), '');
                    img_url = img_url.replace(RegExp('\\?v=.+$','g'), '');
                    $( "#found_thing" ).append('<tr><td>' + data['thing'][i]['title'] + '</td><td>' +
                    data['thing'][i]['categories'] + '</td><td>' + '<img src="https://' + img_url + '">' +
                    '</td></tr>');
                }
                $('table').css('display', 'block');
            }
//            $("#r").html('form');
            $('#spinner').css('display', 'none');
        }
    });
}

function resetData() {
    $('input[name="cancel_button"]').click(function() {
        $('table').css('display', 'none');

    });
}

$(document).ready(function() {
    $('#spinner, #message, table').css('display', 'none');
    getJsonOfThings();
});