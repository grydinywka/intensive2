"""session4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from pizza import views as p_views
from api_expedia.views import delay, GetThingToDoView

urlpatterns = [
    url(r'^$', GetThingToDoView.as_view(), name='thtodo'),
    url(r'^delay/$', delay, name='delay'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/customer-orders/$', p_views.my_admin_view_for_customer, name='customer_order'),
]
