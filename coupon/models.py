from __future__ import unicode_literals

from django.db import models

class Company(models.Model):
    name = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = "Company"
        verbose_name_plural = "Companies"

class Stock(models.Model):
    company = models.ForeignKey('Company', blank=False, null=True, default=None)
    validity_date = models.DateField(blank=False, null=True, default=None)
    description = models.CharField(max_length=254, blank=False, null=True, default=None)
    comment = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"Stock #%s of company %s" % (self.id, self.company)

    class Meta:
        verbose_name = "Stock"
        verbose_name_plural = "Stocks"

class Coupon(models.Model):
    stock = models.ForeignKey('Stock', blank=False, null=True, default=None)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    special_offer = models.IntegerField()
    description = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"Coupon #%s" % self.id

    class Meta:
        verbose_name = "Coupon"
        verbose_name_plural = "Coupons"

class Client(models.Model):
    name = models.CharField(max_length=254, blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    def value_purchase(self):
        purchase = Purchase.objects.filter(client=self)
        return purchase.count()

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"

class Purchase(models.Model):
    coupon = models.ForeignKey('Coupon', blank=False, null=True, default=None)
    client = models.ForeignKey('Client', blank=False, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        purchase = self.client.value_purchase() + 1
        return u"Purchase #%s of %s" % (purchase, self.client)

    class Meta:
        verbose_name = "Purchase"
        verbose_name_plural = "Purchases"