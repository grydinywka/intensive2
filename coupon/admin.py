from django.contrib import admin

from models import Company, Stock, Coupon, Client, Purchase

admin.site.register(Company)
admin.site.register(Stock)
admin.site.register(Coupon)
admin.site.register(Client)
admin.site.register(Purchase)

