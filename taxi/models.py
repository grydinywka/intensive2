from __future__ import unicode_literals

from django.db import models

class Subscriber(models.Model):
    name = models.CharField(
        max_length=256,
        blank=False
    )
    email = models.EmailField(
        default=u"user@gmail.com"
    )
    value_orders = models.IntegerField(
        default=0
    )

    def __unicode__(self):
        return u"%s, id=%s" % (self.name, self.pk,)

class Order(models.Model):
    subscriber = models.ForeignKey(
        "Subscriber",
        blank=False,
        null=True,
        on_delete=models.PROTECT
    )
    value_passenger = models.PositiveSmallIntegerField()
    baggage = models.BooleanField()
    address_from = models.CharField(
        max_length=256,
        blank=False
    )
    address_to = models.CharField(
        max_length=256,
        blank=False
    )
    departure = models.DateTimeField()
    price = models.DecimalField(max_digits=5, decimal_places=2)

    def __unicode__(self):
        return u"Order %s from %s" % (self.subscriber.value_orders, self.subscriber.name,)

    def save(self, *args, **kwargs):
        self.subscriber.value_orders += 1
        self.subscriber.save()
        if self.subscriber.value_orders % 5 == 0:
            self.price *= 0.75

        super(Order, self).save(*args, **kwargs)

class Taken_order(models.Model):
    order = models.ForeignKey(
        "Order",
        blank=False,
        null=True
    )

    def __unicode__(self):
        return u"Order %s is taken" % (self.order.pk)

class Executed_order(models.Model):
    order = models.ForeignKey(
        "Order",
        blank=False,
        null=True
    )

    def __unicode__(self):
        return u"Order %s is executed" % (self.order.pk)