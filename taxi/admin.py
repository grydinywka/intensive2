from django.contrib import admin
from .models import Subscriber, Order, Taken_order, Executed_order


admin.site.register(Subscriber)
admin.site.register(Order)
admin.site.register(Taken_order)
admin.site.register(Executed_order)
