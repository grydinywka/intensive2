from __future__ import unicode_literals

from django.apps import AppConfig


class TourCrmConfig(AppConfig):
    name = 'tour_crm'
