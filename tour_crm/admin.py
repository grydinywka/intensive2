from django.contrib import admin
from .models import Country, Hotel, Location, Tour, Contact, Client, StatusOrder, Order

admin.site.register(Country)
admin.site.register(Hotel)
admin.site.register(Location)
admin.site.register(Tour)
admin.site.register(Contact)
admin.site.register(Client)
admin.site.register(StatusOrder)
admin.site.register(Order)
