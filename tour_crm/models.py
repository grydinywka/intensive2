from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError


class Country(models.Model):
    name = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Country"
    )

    def __unicode__(self):
        return u"%s" % self.name

class Hotel(models.Model):
    title = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Title"
    )
    level = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Stars",
        default=1,
        choices=(
            (u"One",1),
            (u"Two",2),
            (u"Three",3),
            (u"Four",4),
            (u"Five",5)
        )
    )

    country = models.ForeignKey(
        'Country',
        blank=False,
        null=True,
        on_delete=models.SET_NULL
    )

    def __unicode__(self):
        return u"%s %s" % (self.title, self.level)


class Location(models.Model):
    hotel = models.ForeignKey(
        "Hotel",
        blank=False,
        null=True,
        on_delete=models.SET_NULL
    )

    def __unicode__(self):
        return u"Location: %s, Hotel: %s" % (self.hotel.country.name, self.hotel.title)

class Tour(models.Model):
    title = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Tour Name"
    )

    begin_tour = models.DateField()
    end_tour = models.DateField()

    location = models.ForeignKey(
        "Location",
        blank=False,
        null=True,
        on_delete=models.SET_NULL
    )

    price = models.IntegerField(
        default= 1000
    )

    places = models.IntegerField()

    available_places = models.IntegerField()

    def __unicode__(self):
        return u"Tour: %s, %s, %s-%s" % (self.title, self.location, self.begin_tour, self.end_tour)

class Client(models.Model):
    name = models.CharField(
        max_length=256,
        blank=False
    )
    insurance = models.BooleanField()
    age = models.DecimalField(
        max_digits=3,
        decimal_places=0
    )

    def __unicode__(self):
        return (u"{} {}").format(self.name, self.age)

class Contact(models.Model):
    """Contact model"""

    class Meta(object):
        verbose_name = u"Contact"
        verbose_name_plural = u"Contacts"

    type = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u'kind of contact',
        choices=(
            (u'Email', u'Email'),
            (u'Mobile number', u'Mobile number'),
        )
    )
    contact_value = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Contact"
    )

    client = models.ForeignKey('Client',
                               verbose_name=u"Client",
                               blank=False,
                               null=True,
                               on_delete=models.PROTECT)

    def clean(self):
        data = self.contact_value

        if self.type == u'Mobile number':
            try:
                if len(data) != 10:
                    raise ValidationError(u"Number is not valid")
                data = int(data)
            except Exception as e:
                raise ValidationError(e)
        elif self.type == u'Email':
            from django.core.validators import validate_email
            validate_email(data)

    def __unicode__(self):
        return (u"{} {}").format(self.type, self.contact_value)

class StatusOrder(models.Model):
    title = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Status of Order"
    )

    def __unicode__(self):
        return u"%s" % (self.title, )

class Order(models.Model):
    client = models.ForeignKey(
        'Client',
        verbose_name=u"Client",
        blank=False,
        null=True,
        on_delete=models.SET_NULL
    )

    tour = models.ForeignKey(
        'Tour',
        blank=False,
        verbose_name=u"Tour",
        null=True,
        on_delete=models.SET_NULL
    )

    status = models.ForeignKey(
        'StatusOrder',
        blank=False,
        verbose_name=u"Status",
        null=True,
        on_delete=models.SET_NULL
    )

    places = models.IntegerField()

    def __unicode__(self):
        return u"Order of %s" % (self.client.name, )