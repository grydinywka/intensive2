# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-26 10:55
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tour_crm', '0009_tour_clients'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='client',
        ),
        migrations.RemoveField(
            model_name='tour',
            name='clients',
        ),
        migrations.RemoveField(
            model_name='tour',
            name='location',
        ),
        migrations.DeleteModel(
            name='Client',
        ),
        migrations.DeleteModel(
            name='Contact',
        ),
        migrations.DeleteModel(
            name='Location',
        ),
        migrations.DeleteModel(
            name='Tour',
        ),
    ]
